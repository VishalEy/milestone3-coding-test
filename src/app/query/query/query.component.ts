import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

export class Contact {
  constructor(
    public firstname: string,
    public lastname: string,
    public phonenumber: number,
    public email: string,
    public message: string
  ) { }
}

@Component({
  selector: 'app-query',
  templateUrl: './query.component.html',
  styleUrls: ['./query.component.scss']
})
export class QueryComponent implements OnInit {
  @Output() contactdata = new EventEmitter<Contact>();
  public contactForm :any ;
  public obj: any = {};
  constructor(private fb: FormBuilder) { 
    contactForm: FormGroup ;

}
ngOnInit() {
  this.contactForm = this.fb.group({
    firstname: ["", [Validators.required]],
    lastname: ["", [Validators.required]],
    phonenumber: ["", [Validators.required]],
    email: ["", [Validators.required,Validators.pattern("[^ @]*@[^ @]*")]],
    message:["",[Validators.required]]
  });
}

onSubmit() {
  this.obj = { ...this.contactForm.value, ...this.obj };
  this.contactForm.value;
  console.log(
    "LOG: LoginComponent -> onSubmit -> this.contactForm.value",
    this.contactForm.value
  );

  if (this.contactForm.valid) {
    this.contactdata.emit(
      new Contact(
        this.contactForm.value.firstname,
        this.contactForm.value.lastname,
        this.contactForm.value.phonenumber,
        this.contactForm.value.email,
        this.contactForm.value.message
      )
    );
  }

}
}



  

