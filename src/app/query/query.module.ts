import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QueryRoutingModule } from './query-routing.module';
import { QueryComponent } from './query/query.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    QueryComponent
  ],
  imports: [
    CommonModule,
    QueryRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class QueryModule { }
