import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

import { PlaceAppointmentComponent } from './place-appointment.component';

describe('PlaceAppointmentComponent', () => {
  let component: PlaceAppointmentComponent;
  let fixture: ComponentFixture<PlaceAppointmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      providers: [

        HttpClient,

        FormBuilder

      ],

      imports: [

        RouterTestingModule,

        HttpClientModule

      ]

    })
    .compileComponents();

    fixture = TestBed.createComponent(PlaceAppointmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
