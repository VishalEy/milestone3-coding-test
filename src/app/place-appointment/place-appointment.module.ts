import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlaceAppointmentRoutingModule } from './place-appointment-routing.module';
import { PlaceAppointmentComponent } from './place-appointment/place-appointment.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    PlaceAppointmentComponent
  ],
  imports: [
    CommonModule,
    PlaceAppointmentRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class PlaceAppointmentModule { }
