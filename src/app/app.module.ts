import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientInterceptor } from './http-client.interceptor';
import { PlaceAppointmentModule } from './place-appointment/place-appointment.module';
import { QueryModule } from './query/query.module';
import { ViewModule } from './view/view.module';
import { WelcomeModule } from './welcome/welcome.module';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PlaceAppointmentModule,
    QueryModule,
    ViewModule,
    WelcomeModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    {

      provide: HTTP_INTERCEPTORS,

      useClass:HttpClientInterceptor,

      multi:true

      }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
