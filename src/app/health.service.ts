import { Injectable } from '@angular/core';
import { HttpClient,HttpParams,HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class HealthService {
  constructor(private http: HttpClient) { }  
  url : string = "http://localhost:3000/appointments";
  
  public getString (object: any) {
    let paramsAndValues = [];
    for(let key in object) {
      let value = encodeURIComponent(object[key].toString());
      paramsAndValues.push([key, value].join('='));
    }
    return paramsAndValues.join('&');
  }

  public postStringHold (object: any) {
    var body = new HttpParams();
    for(let key in object) {
      let value = encodeURIComponent(object[key].toString());
      body = body.append(key,value);
    }
    return body;
  }

  public postString (object: any) {
    var body = new FormData();
    for(let key in object) {
      let value = encodeURIComponent(object[key].toString());
      body.append(key,value);
    }
    return body;
  }

  public toFormData( formValue: any ) {
    const formData = new FormData();
    for ( const key of Object.keys(formValue) ) {
      const value = formValue[key];
      formData.append(key, value);
    }
    return formData;
  }

  get() {

    return this.http.get(this.url);

  }
  
  public post(url: string, data?: any, options?: any) {
    // data = this.postString(data);
    return this.http.post(url, data, options);
  }  
  
  public put(url: string, data?: any, options?: any) {
    return this.http.put(url, data, options);
  }  
  

  public postform(url: string, data?: any, options?: any) {
    data = this.postString(data);
    return this.http.post(url, data, options);
  }

	public upload(url: string, data?: any, options?: any) {
    const formData = new FormData();
		for ( const key of Object.keys(data) ) {
      const value = data[key];
      formData.append(key, value);
    }
    return this.http.post(url, formData);
  }
}
