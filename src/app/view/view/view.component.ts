import { Component, OnInit } from '@angular/core';
import { HealthService } from 'src/app/health.service';

import { Health } from 'src/app/health';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit { posts: HealthService[] = [];

  // posts: HealthService[] = [];

  constructor(public api:HealthService) { }

  data:any;

  ngOnInit(): void {

    this.api.get().subscribe(res=>{

        this.data=res;

    })


  }

}
