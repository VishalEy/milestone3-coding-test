import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PlaceAppointmentComponent } from './place-appointment/place-appointment/place-appointment.component';
import { QueryComponent } from './query/query/query.component';
import { ViewComponent } from './view/view/view.component';
import { HomeComponent } from './welcome/home/home.component';

const routes: Routes = [  {path:'',redirectTo:'home',pathMatch:'full'},
  {path: 'welcome', component:HomeComponent},



{path: 'view', component:ViewComponent},



{path: 'query', component:QueryComponent},



{path: 'placeappointment', component:PlaceAppointmentComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
