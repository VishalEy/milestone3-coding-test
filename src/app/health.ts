export interface Health{
    firstname: string;
  
    lastname: string;
  
    age: number;
  
    phonenumber: number;
  
    email: string;
  
    streetaddress: string;
  
    city: string;
  
    state: string;
  
    country: string;
  
    pincode: number;
  
    trainerpreference: string;
  
    physiotherapist: string;
  
    packages: string;
  
    inr: number;
  
    paisa: number;
  
    id: number;
    }